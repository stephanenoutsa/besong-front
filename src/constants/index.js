export default {
  'app-name': 'Besong Foundation',
  'app-slogan': 'Quick Fix On Demand',
  'app-city': 'Douala',
  'app-postal-code': 'BP 8125',
  'app-phone': '677 171 754',
  'app-email1': 'info@besongfoundation.org',
  'app-email2': 'helen.arrey@besongfoundation.org',
}
