// Layouts
const MainLayout = () => import(/* webpackChunkName: 'main-layout' */ 'layouts/MainLayout.vue')
const ProgramLayout = () => import(/* webpackChunkName: 'program-layout' */ 'layouts/ProgramLayout.vue')

// Pages
const ComingSoon = () => import(/* webpackChunkName: 'coming-soon' */ 'pages/ComingSoon.vue')
const Home = () => import(/* webpackChunkName: 'home' */ 'pages/Home.vue')
const About = () => import(/* webpackChunkName: 'about' */ 'pages/About.vue')
const Events = () => import(/* webpackChunkName: 'events' */ 'pages/Events')
const Donate = () => import(/* webpackChunkName: 'donate' */ 'pages/Donate.vue')
const Contact = () => import(/* webpackChunkName: 'contact' */ 'pages/Contact.vue')
const Page404 = () => import(/* webpackChunkName: '404' */ 'pages/errors/Error404.vue')

const routes = [
  {
    path: '/',
    component: MainLayout,
    children: [
      {
        path: '',
        name: 'home',
        component: Home
      },
      {
        path: 'about',
        name: 'about',
        component: About
      },
      {
        path: 'programs',
        component: ProgramLayout,
        children: [
          {
            path: 'accommodation',
            name: 'accommodation',
            component: ComingSoon
          },
          {
            path: 'vocational-training',
            name: 'vocational-training',
            component: ComingSoon
          },
          {
            path: 'counseling-services',
            name: 'counseling-services',
            component: ComingSoon
          }
        ]
      },
      {
        path: 'events',
        name: 'events',
        component: Events
      },
      {
        path: 'contact',
        name: 'contact',
        component: Contact
      },
      {
        path: 'donate',
        name: 'donate',
        component: Donate
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    name: 'page404',
    component: Page404
  }
]

export default routes
